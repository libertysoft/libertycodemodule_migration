<?php
/**
 * This class allows to define migration collection repository class.
 * Migration collection repository allows to prepare data from migration collection,
 * to save it, using migration entity system.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\model\repository;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\build\model\DefaultBuilder;
use liberty_code\migration\build\directory\model\DirBuilder;
use liberty_code\migration_model\migration\model\MigEntityCollection;
use liberty_code\migration_model\migration\model\repository\MigEntityCollectionRepository;
use liberty_code\migration_model\build\model\DefaultBuilder as MigEntityBuilder;



class MigrationCollectionRepository extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Application instance.
     * @var AppInterface
     */
    protected $objApp;


    /**
     * DI: Migration builder instance.
     * @var DefaultBuilder
     */
    protected $objBuilder;



    /**
     * DI: Migration directory builder instance.
     * @var DirBuilder
     */
    protected $objDirBuilder;



    /**
     * DI: Migration entity collection repository instance.
     * @var MigEntityCollectionRepository
     */
    protected $objMigEntityCollectionRepository;



    /**
     * DI: Migration entity builder instance.
     * @var MigEntityBuilder
     */
    protected $objMigEntityBuilder;



    /**
     * DI: Migration entity collection instance.
     * @var MigEntityCollection
     */
    protected $objMigEntityCollection;



    /**
     * DI: Removed migration entity collection instance.
     * @var MigEntityCollection
     */
    protected $objRemoveMigEntityCollection;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * Constructor.
     *
     * @param AppInterface $objApp
     * @param DefaultBuilder $objBuilder
     * @param DirBuilder $objDirBuilder
     * @param MigEntityCollectionRepository $objMigEntityCollectionRepository
     * @param MigEntityBuilder $objMigEntityBuilder
     * @param MigEntityCollection $objMigEntityCollection
     * @param MigEntityCollection $objRemoveMigEntityCollection
     */
    public function __construct(
        AppInterface $objApp,
        DefaultBuilder $objBuilder,
        DirBuilder $objDirBuilder,
        MigEntityCollectionRepository $objMigEntityCollectionRepository,
        MigEntityBuilder $objMigEntityBuilder,
        MigEntityCollection $objMigEntityCollection,
        MigEntityCollection $objRemoveMigEntityCollection
    )
    {
        // Init properties
        $this->objApp = $objApp;
        $this->objMigEntityCollection = $objMigEntityCollection;
        $this->objRemoveMigEntityCollection = $objRemoveMigEntityCollection;
        $this->objMigEntityCollectionRepository = $objMigEntityCollectionRepository;
        $this->objBuilder = $objBuilder;
        $this->objDirBuilder = $objDirBuilder;
        $this->objMigEntityBuilder = $objMigEntityBuilder;

        // Call parent constructor
        parent::__construct();
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate migration builder.
     */
    protected function hydrateBuilder()
    {
        $this->objBuilder->setTabDataSrc($this->getBuilderTabDataSrc());
    }



    /**
     * Hydrate migration directory builder.
     */
    protected function hydrateDirBuilder()
    {
        $this->objDirBuilder->setTabDataSrc($this->getDirBuilderTabDataSrc());
    }



    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Hydrate migration builders
        $this->hydrateBuilder();
        $this->hydrateDirBuilder();

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get data source array,
     * for migration builder.
     *
     * @return array
     */
    protected function getBuilderTabDataSrc()
    {
        // Init var
        $objConfig = $this->objApp->getObjConfig();
        $strConfigKey = ToolBoxConfig::getStrPathKeyFromList('migration', 'config');
        $result = (
            (
                (!is_null($objConfig)) &&
                $objConfig->checkValueExists($strConfigKey) &&
                is_array($objConfig->getValue($strConfigKey))
            ) ?
                $objConfig->getValue($strConfigKey) :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * Get data source array,
     * for migration directory builder.
     *
     * @return array
     */
    public function getDirBuilderTabDataSrc()
    {
        // Init var
        $objConfig = $this->objApp->getObjConfig();
        $strConfigKey = ToolBoxConfig::getStrPathKeyFromList('migration', 'dir_path');
        $result = (
            (
                (!is_null($objConfig)) &&
                $objConfig->checkValueExists($strConfigKey) &&
                is_array($objConfig->getValue($strConfigKey))
            ) ?
                $objConfig->getValue($strConfigKey) :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * Get migration entity collection,
     * hydrated from specified migration collection.
     *
     * @param MigrationCollectionInterface $objMigrationCollection
     * @return null|MigEntityCollection
     */
    public function getObjMigEntityCollection(MigrationCollectionInterface $objMigrationCollection)
    {
        // Build migration entity collection from migration collection
        $this->objMigEntityBuilder->hydrateMigEntityCollection(
            $this->objMigEntityCollection,
            $objMigrationCollection
        );

        // Return result
        return $this->objMigEntityCollection;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load specified migration collection.
     *
     * @param MigrationCollectionInterface $objMigrationCollection
     * @return boolean
     */
    public function load(MigrationCollectionInterface $objMigrationCollection)
    {
        // Build migration collection
        $this->objDirBuilder->hydrateMigrationCollection($objMigrationCollection);
        $this->objBuilder->hydrateMigrationCollection($objMigrationCollection, false);

        // Init migration entity collections
        $this->objMigEntityCollection->removeItemAll();
        $this->objRemoveMigEntityCollection->removeItemAll();

        // Load migration entity collection
        $result = $this->objMigEntityCollectionRepository->loadAll($this->objMigEntityCollection);

        // Build migration collection from migration entity collection
        $this->objMigEntityBuilder->hydrateMigrationCollection(
            $objMigrationCollection,
            $this->objMigEntityCollection
        );

        // Return result
        return $result;
    }



    /**
     * Save specified migration collection.
     *
     * @param MigrationCollectionInterface $objMigrationCollection
     * @return boolean
     */
    public function save(MigrationCollectionInterface $objMigrationCollection)
    {
        // Init migration entity collection
        $this->objRemoveMigEntityCollection->removeItemAll();

        // Build migration entity collection from migration collection
        $this->objMigEntityBuilder->hydrateMigEntityCollection(
            $this->objMigEntityCollection,
            $objMigrationCollection,
            $this->objRemoveMigEntityCollection
        );

        // Save migration entity collection
        $result = $this->objMigEntityCollectionRepository->save($this->objMigEntityCollection);
        $this->objMigEntityCollectionRepository->remove($this->objRemoveMigEntityCollection);

        // Return result
        return $result;
    }



}