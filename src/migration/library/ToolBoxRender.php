<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\command\request_flow\response\info\library\ToolBoxInfoResponse;
use liberty_code\migration\migration\api\MigrationInterface;
use liberty_code\migration\migration\version\model\VersionMigration;
use liberty_code\migration_model\migration\library\ConstMigration;
use liberty_code\migration_model\migration\model\MigEntity;
use liberty_code\migration_model\migration\model\MigEntityCollection;



class ToolBoxRender extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();

    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    /**
     * Get array of render data,
     * from specified migration object.
     *
     * @param MigrationInterface $objMigration
     * @param MigEntity $objMigEntity = null
     * @return string
     */
    public static function getStrMigrationRender(
        MigrationInterface $objMigration,
        MigEntity $objMigEntity = null
    )
    {
        // Build render array
        $tabRender = array();
        $tabRender[] = ['Key', ':' . $objMigration->getStrKey()];
        if($objMigration instanceof VersionMigration)
        {
            $tabRender[] = ['Version', ':' . $objMigration->getStrVersion()];
            $tabRender[] = ['Order', ':' . strval($objMigration->getIntOrder())];
        }
        $tabRender[] = ['Is executed', ':' . ($objMigration->checkIsExecuted() ? '1' : '0')];
        if((!is_null($objMigEntity)) && (!$objMigEntity->checkIsNew()))
        {
            $tabRender[] = ['Date create', ':' .
                $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_DT_CREATE)];
            $tabRender[] = ['Date update', ':' .
                $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_DT_UPDATE)];
        }

        // Set render
        $result = ToolBoxInfoResponse::getStrTable($tabRender);

        return $result;
    }



    /**
     * Get array of render data,
     * from specified API auth user object collection.
     *
     * @param MigrationInterface[] $tabMigration
     * @param MigEntityCollection $objMigEntityCollection = null
     * @return string
     */
    public static function getStrTabMigrationRender(
        array $tabMigration,
        MigEntityCollection $objMigEntityCollection = null
    )
    {
        // Run each migration
        $tabRender = array(
            ['Key', 'Is executed', 'Date create', 'Date update'],
            ['---', '-----------', '-----------', '-----------'],
        );
        foreach($tabMigration as $objMigration)
        {
            $strKey = $objMigration->getStrKey();
            $objMigEntity = (
                (!is_null($objMigEntityCollection)) ?
                    $objMigEntityCollection->getObjMigEntityFromKey($strKey) :
                    null
            );

            // Build render array
            $tabRender[] = [
                $strKey,
                ($objMigration->checkIsExecuted() ? '1' : '0'),
                (
                    ((!is_null($objMigEntity)) && (!$objMigEntity->checkIsNew())) ?
                        $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_DT_CREATE) :
                        ''
                ),
                (
                    ((!is_null($objMigEntity)) && (!$objMigEntity->checkIsNew())) ?
                        $objMigEntity->getAttributeValue(ConstMigration::ATTRIBUTE_KEY_DT_UPDATE) :
                        ''
                )
            ];
        }

        // Set render
        $result = ToolBoxInfoResponse::getStrTable($tabRender);

        return $result;
    }



}