<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\library\reflection\library\ToolBoxClassReflection;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\migration\migration\version\fix\model\FixVersionMigration;
use liberty_code_module\migration\migration\library\ConstMigration;
use liberty_code_module\migration\migration\exception\FilePathAddInvalidFormatException;



class ToolBoxMigration extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

	/**
     * Add specified migration.
     *
     * @param AppInterface $objApp
     * @param string $strFilePath
     * @param string $strClassPath = null
     * @param string $strParentClassPath = null
     * @param string $strKey = null
     * @param string $strVersion = null
     * @param integer $intOrder = null
     * @throws FilePathAddInvalidFormatException
     */
    public static function add(
        AppInterface $objApp,
        $strFilePath,
        $strClassPath = null,
        $strParentClassPath = null,
        $strKey = null,
        $strVersion = null,
        $intOrder = null
    )
    {
        // Init var
        $objModuleConfig = $objApp
            ->getObjModuleCollection()
            ->getObjModule(ConstMigration::MODULE_KEY)
            ->getObjConfig();

        // Init file path
        $strFileFullPath = $objApp->getStrRootDirPath() . $strFilePath;

        // Set check argument
        FilePathAddInvalidFormatException::setCheck($strFileFullPath);

        // Init class path
        $strClassPath = (
            is_null($strClassPath) ?
                str_replace(
                    '/',
                    '\\',
                    (pathinfo($strFilePath, PATHINFO_DIRNAME) . '\\' . pathinfo($strFilePath, PATHINFO_FILENAME))
                ) :
                $strClassPath
        );

        // Init parent class path
        $strParentClassPath = (
            (
                (!is_null($strParentClassPath)) &&
                (
                    ($strParentClassPath == FixVersionMigration::class) ||
                    (is_subclass_of($strParentClassPath, FixVersionMigration::class))
                )
            ) ?
                $strParentClassPath :
                FixVersionMigration::class
        );

        // Init info
        $strNamespace = ltrim(ToolBoxClassReflection::getStrNamespace($strClassPath), '\\');
        $strClassNm = ToolBoxClassReflection::getStrClassName($strClassPath);
        $strParentClassNm = ToolBoxClassReflection::getStrClassName($strParentClassPath);
        $strKey = (is_null($strKey) ? $strClassNm : $strKey);
        $strVersion = (is_null($strVersion) ? '1.0.0' : $strVersion);
        $intOrder = (is_null($intOrder) ? 0 : $intOrder);

        // Build migration file content
        $strTmpFilePath = $objModuleConfig->getValue('template/migration_path');
        $strContent = file_get_contents($strTmpFilePath);
        $strContent = str_replace(
            array(
                '__namespace__',
                '__class_name__',
                '__parent_class_path__',
                '__parent_class_name__',
                '__key__',
                '__version__',
                '__order__'
            ),
            array(
                $strNamespace,
                $strClassNm,
                $strParentClassPath,
                $strParentClassNm,
                $strKey,
                $strVersion,
                $intOrder
            ),
            $strContent
        );

        // Create migration file
        file_put_contents($strFileFullPath, $strContent);
    }



}