<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\library;



class ConstMigration
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'migration_migration';

	// Configuration command line arguments
	const COMMAND_ARG_NAME_KEY = 'key';
    const COMMAND_OPT_NAME_FORCE = 'force';
    const COMMAND_OPT_NAME_REGEXP_KEY = 'regexp-key';
    const COMMAND_OPT_NAME_LIST_KEY = 'list-key';
    const COMMAND_OPT_NAME_SORT_COMPARE_KEY = 'sort-key';
    const COMMAND_OPT_NAME_REGEXP_VERSION = 'regexp-version';
    const COMMAND_OPT_NAME_LIST_VERSION = 'list-version';
    const COMMAND_OPT_NAME_SORT_COMPARE_VERSION = 'sort-version';
    const COMMAND_OPT_NAME_REGEXP_ORDER = 'regexp-order';
    const COMMAND_OPT_NAME_LIST_ORDER = 'list-order';
    const COMMAND_OPT_NAME_SORT_COMPARE_ORDER = 'sort-order';
    const COMMAND_OPT_NAME_IS_EXECUTED = 'is-executed';
    const COMMAND_OPT_NAME_EXECUTE_IS_EXECUTED = 'execute-is-executed';
    const COMMAND_OPT_NAME_ROLLBACK_IS_EXECUTED = 'rollback-is-executed';
    const COMMAND_OPT_NAME_SET_IS_NOT_EXECUTED = 'set-is-not-executed';
    const COMMAND_ARG_NAME_FILE_PATH = 'file-path';
    const COMMAND_OPT_NAME_CLASS_PATH = 'class-path';
    const COMMAND_OPT_NAME_PARENT_CLASS_PATH = 'parent-class-path';
    const COMMAND_OPT_NAME_KEY = 'key';
    const COMMAND_OPT_NAME_VERSION = 'version';
    const COMMAND_OPT_NAME_ORDER = 'order';



    // Exception message constants
    const EXCEPT_MSG_FILE_PATH_ADD_INVALID_FORMAT =
        'Following migration file path "%1$s" invalid! The file path must be a valid string file full path and not exists.';
}