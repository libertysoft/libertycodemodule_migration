<?php
/**
 * This class allows to define fixed command migration class.
 * Fixed command migration is fixed version migration, using comments on execution and rollback.
 *
 * Fixed command migration uses the following specified configuration:
 * [
 *     Fixed version migration configuration
 * ]
 *
 * Fixed command migration uses the following specified selection configuration, to be selected:
 * [
 *     Fixed version migration selection configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\command\fix\model;

use liberty_code\migration\migration\version\fix\model\FixVersionMigration;



abstract class FixCommandMigration extends FixVersionMigration
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute($boolForce = false)
    {
        // Comment
        echo(sprintf(
            'Migration "%1$s"  to execute.' . PHP_EOL,
            $this->getStrKey()
        ));

        // Call parent method
        parent::execute($boolForce);

        // Comment
        echo(sprintf(
            'Migration "%1$s" executed.' . PHP_EOL,
            $this->getStrKey()
        ));
    }



    /**
     * @inheritdoc
     */
    public function rollback($boolForce = false)
    {
        // Comment
        echo(sprintf(
            'Migration "%1$s" to rollback.' . PHP_EOL,
            $this->getStrKey()
        ));

        // Call parent method
        parent::rollback($boolForce);

        // Comment
        echo(sprintf(
            'Migration "%1$s" rollback.' . PHP_EOL,
            $this->getStrKey()
        ));
    }



}