<?php
/**
 * This class allows to define command migration class.
 * Command migration is version migration, using comments on execution and rollback.
 *
 * Command migration uses the following specified configuration:
 * [
 *     Version migration configuration
 * ]
 *
 * Command migration uses the following specified selection configuration, to be selected:
 * [
 *     Version migration selection configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\command\model;

use liberty_code\migration\migration\version\model\VersionMigration;



abstract class CommandMigration extends VersionMigration
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function execute($boolForce = false)
    {
        // Comment
        echo(sprintf(
            'Migration "%1$s"  to execute.' . PHP_EOL,
            $this->getStrKey()
        ));

        // Call parent method
        parent::execute($boolForce);

        // Comment
        echo(sprintf(
            'Migration "%1$s" executed.' . PHP_EOL,
            $this->getStrKey()
        ));
    }



    /**
     * @inheritdoc
     */
    public function rollback($boolForce = false)
    {
        // Comment
        echo(sprintf(
            'Migration "%1$s" to rollback.' . PHP_EOL,
            $this->getStrKey()
        ));

        // Call parent method
        parent::rollback($boolForce);

        // Comment
        echo(sprintf(
            'Migration "%1$s" rollback.' . PHP_EOL,
            $this->getStrKey()
        ));
    }



}