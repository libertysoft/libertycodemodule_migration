<?php
/**
 * Description :
 * This class allows to define migration controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\controller\command;

use liberty_code\controller\controller\model\DefaultController;

use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\migration\migration\library\ConstMigration as BaseConstMigration;
use liberty_code\migration\migrator\api\MigratorInterface;
use liberty_code\migration\migration\version\library\ConstVersionMigration;
use liberty_code_module\migration\migration\model\repository\MigrationCollectionRepository;
use liberty_code_module\migration\migration\library\ConstMigration;
use liberty_code_module\migration\migration\library\ToolBoxMigration;
use liberty_code_module\migration\migration\library\ToolBoxRender;



class MigrationController extends DefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Application
     * @var AppInterface
     */
    protected $objApp;



    /**
     * DI: Migrator instance.
     * @var MigratorInterface
     */
    protected $objMigrator;



    /**
     * DI: Migration collection repository instance.
     * @var MigrationCollectionRepository
     */
    protected $objMigrationCollectionRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param MigratorInterface $objMigrator
     * @param MigrationCollectionRepository $objMigrationCollectionRepository
     */
    public function __construct(
        AppInterface $objApp,
        MigratorInterface $objMigrator,
        MigrationCollectionRepository $objMigrationCollectionRepository
    )
    {
        // Init properties
        $this->objApp = $objApp;
        $this->objMigrator = $objMigrator;
        $this->objMigrationCollectionRepository = $objMigrationCollectionRepository;

        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        // Return result
        return true;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get command line option configuration,
     * for migrations selection.
     *
     * @param CommandFrontController $objFrontController
     * @return null|array
     */
    protected function getTabCmdOptConfig(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $result = array();

        // Get REGEXP configuration
        $tabConfig = array(
            BaseConstMigration::TAB_GET_CONFIG_KEY_REGEXP_KEY =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_REGEXP_KEY, null),
            ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_VERSION =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_REGEXP_VERSION, null),
            ConstVersionMigration::TAB_GET_CONFIG_KEY_REGEXP_ORDER =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_REGEXP_ORDER, null)
        );
        foreach($tabConfig as $strKey => $strConfig)
        {
            // Register configuration, if required
            if(!is_null($strConfig))
            {
                $result[$strKey] = $strConfig;
            }
        }

        // Get list configuration
        $tabConfig = array(
            BaseConstMigration::TAB_GET_CONFIG_KEY_LIST_KEY =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_LIST_KEY, null),
            ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_VERSION =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_LIST_VERSION, null),
            ConstVersionMigration::TAB_GET_CONFIG_KEY_LIST_ORDER =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_LIST_ORDER, null)
        );
        foreach($tabConfig as $strKey => $strConfig)
        {
            // Register configuration, if required
            if(!is_null($strConfig))
            {
                $result[$strKey] = explode(',', $strConfig);
            }
        }

        // Get sort comparison configuration
        $tabConfig = array(
            BaseConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_KEY =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_KEY, null),
            ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VERSION =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_VERSION, null),
            ConstVersionMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_ORDER =>
                $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_ORDER, null)
        );
        foreach($tabConfig as $strKey => $strConfig)
        {
            // Register configuration, if required
            if(!is_null($strConfig))
            {
                // Run each conditions
                $tabSubConfig = explode(',', $strConfig);
                foreach($tabSubConfig as $strSubConfig)
                {
                    $subConfig = explode(':', $strSubConfig);

                    if(!array_key_exists($strKey, $result))
                    {
                        $result[$strKey] = array();
                    }

                    if(count($subConfig) > 1)
                    {
                        $result[$strKey][] = array(
                            BaseConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_OPERATOR => $subConfig[0],
                            BaseConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE => $subConfig[1]
                        );
                    }
                    else
                    {
                        $result[$strKey][] = array(
                            BaseConstMigration::TAB_GET_CONFIG_KEY_SORT_COMPARE_VALUE => $subConfig[0]
                        );
                    }
                }
            }
        }

        // Return result
        $result = ((count($result) > 0) ? $result : null);
        return $result;
    }



    /**
     * Get command line option is executed,
     * for migrations selection.
     *
     * @param CommandFrontController $objFrontController
     * @param string $strCmdOptName
     * @param null|boolean $default
     * @return null|boolean
     */
    protected function getCmdOptIsExecuted(
        CommandFrontController $objFrontController,
        $strCmdOptName,
        $default = null
    )
    {
        // Init var
        $result = $objFrontController->getOptValue($strCmdOptName, $default);
        $result = (
            is_string($result) ?
                (
                    (($result == 'true') || ($result == '1')) ?
                        true :
                        (
                            (($result == 'false') || ($result == '0')) ?
                                false :
                                null
                        )
                ):
                $result
        );

        // Return result
        return $result;
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load migration.
     *
     * @return boolean
     */
    protected function loadMigration()
    {
        // Load migration collection
        $result = $this->objMigrationCollectionRepository->load(
            $this->objMigrator->getObjMigrationCollection()
        );

        // Return result
        return $result;
    }



    /**
     * Save migration.
     *
     * @return boolean
     */
    protected function saveMigration()
    {
        // Load migration collection
        $result = $this->objMigrationCollectionRepository->save(
            $this->objMigrator->getObjMigrationCollection()
        );

        // Return result
        return $result;
    }





    // Methods action
    // ******************************************************************************

    /**
     * Execute migration.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionExecuteMigration(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strKey = $objFrontController->getArgValue(ConstMigration::COMMAND_ARG_NAME_KEY);
        $boolForce = ($objFrontController->getOptValue(
            ConstMigration::COMMAND_OPT_NAME_FORCE,
            false
        ) !== false);

        // Load migration
        $this->loadMigration();

        // Execute migration
        $this->objMigrator->executeMigration($strKey, $boolForce);

        // Save migration
        $this->saveMigration();

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Execute migrations.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionExecute(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $tabConfig = $this->getTabCmdOptConfig($objFrontController);
        $isExecuted = $this->getCmdOptIsExecuted(
            $objFrontController,
            ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED,
            false
        );

        // Load migration
        $this->loadMigration();

        // Execute migrations
        $this->objMigrator->execute($tabConfig, $isExecuted);

        // Save migration
        $this->saveMigration();

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Rollback migration.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionRollbackMigration(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strKey = $objFrontController->getArgValue(ConstMigration::COMMAND_ARG_NAME_KEY);
        $boolForce = ($objFrontController->getOptValue(
            ConstMigration::COMMAND_OPT_NAME_FORCE,
            false
        ) !== false);

        // Load migration
        $this->loadMigration();

        // Rollback migration
        $this->objMigrator->rollbackMigration($strKey, $boolForce);

        // Save migration
        $this->saveMigration();

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Rollback migrations.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionRollback(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $tabConfig = $this->getTabCmdOptConfig($objFrontController);
        $isExecuted = $this->getCmdOptIsExecuted(
            $objFrontController,
            ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED,
            true
        );

        // Load migration
        $this->loadMigration();

        // Rollback migrations
        $this->objMigrator->rollback($tabConfig, $isExecuted);

        // Save migration
        $this->saveMigration();

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Refresh migration.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionRefreshMigration(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strKey = $objFrontController->getArgValue(ConstMigration::COMMAND_ARG_NAME_KEY);
        $boolForce = ($objFrontController->getOptValue(
            ConstMigration::COMMAND_OPT_NAME_FORCE,
            false
        ) !== false);

        // Load migration
        $this->loadMigration();

        // Refresh migration
        $this->objMigrator->refreshMigration($strKey, $boolForce);

        // Save migration
        $this->saveMigration();

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Refresh migrations.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionRefresh(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $tabConfig = $this->getTabCmdOptConfig($objFrontController);
        $executeIsExecuted = $this->getCmdOptIsExecuted(
            $objFrontController,
            ConstMigration::COMMAND_OPT_NAME_EXECUTE_IS_EXECUTED,
            false
        );
        $rollbackIsExecuted = $this->getCmdOptIsExecuted(
            $objFrontController,
            ConstMigration::COMMAND_OPT_NAME_ROLLBACK_IS_EXECUTED,
            true
        );

        // Load migration
        $this->loadMigration();

        // Refresh migrations
        $this->objMigrator->refresh($tabConfig, $rollbackIsExecuted, $executeIsExecuted);

        // Save migration
        $this->saveMigration();

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Show migration.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionShowMigration(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strKey = $objFrontController->getArgValue(ConstMigration::COMMAND_ARG_NAME_KEY);

        // Load migration
        $this->loadMigration();

        // Get migration
        $objMigration = $this->objMigrator->getObjMigration($strKey);

        // Get migration entity
        $objMigrationCollection = $this->objMigrator->getObjMigrationCollection();
        $objMigEntityCollection = $this
            ->objMigrationCollectionRepository
            ->getObjMigEntityCollection($objMigrationCollection);
        $objMigEntity = $objMigEntityCollection->getObjMigEntityFromKey($objMigration->getStrKey());

        // Get render
        $strRender = ToolBoxRender::getStrMigrationRender($objMigration, $objMigEntity);

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



    /**
     * Show list of migrations.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionShow(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $tabConfig = $this->getTabCmdOptConfig($objFrontController);
        $isExecuted = $this->getCmdOptIsExecuted(
            $objFrontController,
            ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED,
            null
        );
        $objMigrationCollection = $this->objMigrator->getObjMigrationCollection();

        // Load migration
        $this->loadMigration();

        // Get migrations
        $tabMigration = array();
        $tabKey = $objMigrationCollection->getTabSortKey($tabConfig, true, $isExecuted);
        foreach($tabKey as $strKey)
        {
            $tabMigration[] = $objMigrationCollection->getObjMigration($strKey);
        }

        // Get migration entity collection
        $objMigEntityCollection = $this
            ->objMigrationCollectionRepository
            ->getObjMigEntityCollection($objMigrationCollection);

        // Get render
        $strRender = ToolBoxRender::getStrTabMigrationRender($tabMigration, $objMigEntityCollection);

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



    /**
     * Update migrations as is executed or not.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionSetIsExecuted(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $tabConfig = $this->getTabCmdOptConfig($objFrontController);
        $isExecuted = $this->getCmdOptIsExecuted(
            $objFrontController,
            ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED,
            false
        );
        $boolSetIsExecuted = (
            $objFrontController->getOptValue(
                ConstMigration::COMMAND_OPT_NAME_SET_IS_NOT_EXECUTED,
                false
            ) === false
        );
        $objMigrationCollection = $this->objMigrator->getObjMigrationCollection();

        // Load migration
        $this->loadMigration();

        // Get migrations and run each migration selected and set is executed status
        $tabMigration = array();
        $tabKey = $objMigrationCollection->getTabSortKey($tabConfig, true, $isExecuted);
        foreach($tabKey as $strKey)
        {
            $objMigration = $objMigrationCollection->getObjMigration($strKey);
            $objMigration->setIsExecuted($boolSetIsExecuted);
            $tabMigration[] = $objMigration;
        }

        // Save migration
        $this->saveMigration();

        // Get migration entity collection
        $objMigEntityCollection = $this
            ->objMigrationCollectionRepository
            ->getObjMigEntityCollection($objMigrationCollection);

        // Get render
        $strRender = ToolBoxRender::getStrTabMigrationRender($tabMigration, $objMigEntityCollection);

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL . '[DONE]');

        // Return response
        return $objResponse;
    }



    /**
     * Add new migration.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionAdd(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $strFilePath = $objFrontController->getArgValue(ConstMigration::COMMAND_ARG_NAME_FILE_PATH);
        $strClassPath = $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_CLASS_PATH, null);
        $strParentClassPath = $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_PARENT_CLASS_PATH, null);
        $strKey = $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_KEY, null);
        $strVersion = $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_VERSION, null);
        $intOrder = $objFrontController->getOptValue(ConstMigration::COMMAND_OPT_NAME_ORDER, null);

        // Add migration
        ToolBoxMigration::add(
            $this->objApp,
            $strFilePath,
            $strClassPath,
            $strParentClassPath,
            $strKey,
            $strVersion,
            $intOrder
        );

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



}