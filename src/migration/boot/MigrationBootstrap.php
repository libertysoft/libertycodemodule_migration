<?php
/**
 * Description :
 * This class allows to define migration module bootstrap class.
 * Migration module bootstrap allows to boot migration module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityRepository;
use liberty_code_module\migration\migration\library\ConstMigration;



class MigrationBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * DI: Migration entity repository instance.
     * @var SqlMigEntityRepository
     */
    protected $objMigEntityRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param SqlMigEntityRepository $objMigEntityRepository
     */
    public function __construct(
        AppInterface $objApp,
        SqlMigEntityRepository $objMigEntityRepository
    )
    {
        // Init properties
        $this->objMigEntityRepository = $objMigEntityRepository;

        // Call parent constructor
        parent::__construct($objApp, ConstMigration::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set migration structure, if required.
        $this->objMigEntityRepository->setStructure();
    }



}