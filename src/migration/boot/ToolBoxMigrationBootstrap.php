<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\migration\migration\boot;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\bootstrap\library\ToolBoxBootstrap;
use liberty_code\framework\framework\config\library\ToolBoxConfig;



class ToolBoxMigrationBootstrap extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

	/**
     * Put migration configuration,
     * from specified configuration full file path.
     *
     * @param AppInterface $objApp
     * @param ModuleInterface $objModule
     * @param string $strFilePath
     * @return boolean
     */
    public static function putMigrationConfigFile(
        AppInterface $objApp,
        ModuleInterface $objModule,
        $strFilePath
    )
    {
        // Init var
        $result = ToolBoxBootstrap::putAppConfigFile(
            $objApp,
            $objModule,
            $strFilePath,
            'migration'
        );

        // Return result
        return $result;
    }



    /**
     * Add migration directory full path.
     *
     * @param AppInterface $objApp
     * @param string $strDirPath
     * @return boolean
     */
    public static function addMigrationDirPath(
        AppInterface $objApp,
        $strDirPath
    )
    {
        // Init var
        $result = false;
        $objConfig = $objApp->getObjConfig();
        $strConfigKey = ToolBoxConfig::getStrPathKeyFromList('migration', 'dir_path');

        // Set directory path on config
        if(!is_null($objConfig))
        {
            // Init array of directory paths
            $tabDirPath = $objConfig->getValue($strConfigKey);
            $tabDirPath = (is_array($tabDirPath) ? $tabDirPath : array());

            // Register new directory path
            $tabDirPath[] = $strDirPath;

            // Reset array of directory paths on config
            ToolBoxBootstrap::putAppConfigConfigValue(
                $objApp,
                $strConfigKey,
                $tabDirPath
            );
        }

        // Return result
        return $result;
    }



}