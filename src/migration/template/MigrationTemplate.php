<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace __namespace__;

use __parent_class_path__;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\version\library\ConstVersionMigration;



class __class_name__ extends __parent_class_name__
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstMigration::TAB_CONFIG_KEY_KEY => '__key__',
            ConstVersionMigration::TAB_CONFIG_KEY_VERSION => '__version__',
            ConstVersionMigration::TAB_CONFIG_KEY_ORDER => intval('__order__'),
            ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY => true
        );
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function executeMigration()
    {
        // TODO
    }



    /**
     * @inheritdoc
     */
    protected function rollbackMigration()
    {
        // TODO
    }



}