<?php

use liberty_code\migration\build\model\DefaultBuilder;
use liberty_code\migration\build\directory\model\DirBuilder;



return array(
    // Migration configuration
    // ******************************************************************************

    'migration' => [
        /**
         * Configuration used by default builder,
         * to populate migration collection.
         *
         * Format: @see DefaultBuilder data source format
         */
        'config' => [],

        /**
         * Configuration used by directory builder,
         * to populate migration collection.
         *
         * Format: @see DirBuilder data source format
         */
        'dir_path' => []
    ]
);