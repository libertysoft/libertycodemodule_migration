<?php

use liberty_code_module\migration\migration\boot\MigrationBootstrap;



return array(
    'migration_bootstrap' => [
        'call' => [
            'class_path_pattern' => MigrationBootstrap::class . ':boot'
        ]
    ]
);