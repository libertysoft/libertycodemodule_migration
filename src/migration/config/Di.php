<?php

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\migration\migration\factory\api\MigrationFactoryInterface;
use liberty_code\migration\migration\factory\model\DefaultMigrationFactory;
use liberty_code\migration\build\api\BuilderInterface;
use liberty_code\migration\migration\api\MigrationCollectionInterface;
use liberty_code\migration\migration\model\DefaultMigrationCollection;
use liberty_code\migration\migrator\api\MigratorInterface;
use liberty_code\migration\migrator\model\DefaultMigrator;
use liberty_code\migration\build\model\DefaultBuilder;
use liberty_code\migration\build\directory\model\DirBuilder;
use liberty_code\migration_model\migration\model\MigEntityCollection;
use liberty_code\migration_model\migration\sql\model\SqlMigEntityFactory;
use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityRepository;
use liberty_code\migration_model\migration\sql\model\repository\SqlMigEntityCollectionRepository;
use liberty_code\migration_model\build\api\BuilderInterface as MigEntityBuilderInterface;
use liberty_code\migration_model\build\model\DefaultBuilder as MigEntityBuilder;
use liberty_code_module\migration\migration\model\repository\MigrationCollectionRepository;



return array(
    // Migration services
    // ******************************************************************************

    'migration_factory' => [
        'source' => DefaultMigrationFactory::class,
        'argument' => [
            ['type' => 'mixed', 'value' => null],
            ['type' => 'mixed', 'value' => null],
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'migration_builder' => [
        'source' => DefaultBuilder::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'migration_factory']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'migration_directory_builder' => [
        'source' => DirBuilder::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'migration_factory']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'migrator' => [
        'source' => DefaultMigrator::class,
        'argument' => [
            ['type' => 'class', 'value' => DefaultMigrationCollection::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Migration entity services
    // ******************************************************************************

    'migration_entity_factory' => [
        'source' => SqlMigEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'migration_entity_repository' => [
        'source' => SqlMigEntityRepository::class,
        'argument' => [
            ['type' => 'class', 'value' => TablePersistor::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'migration_entity_collection_repository' => [
        'source' => SqlMigEntityCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'migration_entity_factory'],
            ['type' => 'dependency', 'value' => 'migration_entity_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'migration_entity_builder' => [
        'source' => MigEntityBuilder::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'migration_entity_factory']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Module migration services
    // ******************************************************************************

    'migration_collection_repository' => [
        'source' => MigrationCollectionRepository::class,
        'argument' => [
            ['type' => 'class', 'value' => AppInterface::class],
            ['type' => 'dependency', 'value' => 'migration_builder'],
            ['type' => 'dependency', 'value' => 'migration_directory_builder'],
            ['type' => 'dependency', 'value' => 'migration_entity_collection_repository'],
            ['type' => 'dependency', 'value' => 'migration_entity_builder'],
            ['type' => 'class', 'value' => MigEntityCollection::class],
            ['type' => 'class', 'value' => MigEntityCollection::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    MigrationCollectionInterface::class => [
        'set' => ['type' => 'class', 'value' => DefaultMigrationCollection::class]
    ],

    MigrationFactoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'migration_factory'],
        'option' => [
            'shared' => true,
        ]
    ],

    BuilderInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'migration_builder'],
        'option' => [
            'shared' => true,
        ]
    ],

    MigratorInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'migrator'],
        'option' => [
            'shared' => true,
        ]
    ],

    MigEntityBuilderInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'migration_entity_builder'],
        'option' => [
            'shared' => true,
        ]
    ]
);