<?php

use liberty_code_module\migration\migration\library\ConstMigration;
use liberty_code_module\migration\migration\controller\command\MigrationController;



return array(
    // Migration routes
    // ******************************************************************************

    'migration_execute_migration' => [
        'source' => 'migrate:execute:migration',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionExecuteMigration'
        ],
        'description' => 'Execute specified migration',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstMigration::COMMAND_ARG_NAME_KEY,
                'description' => 'Migration key',
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_FORCE, 'f'],
                'description' => 'Force access in case migration already executed',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ]
    ],
    'migration_execute' => [
        'source' => 'migrate:execute',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionExecute'
        ],
        'description' => 'Execute migrations',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_KEY],
                'description' => 'REGEXP pattern key to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_KEY, 'k'],
                'description' => 'Comma separated list of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_KEY],
                'description' => 'Comma separated list of sort conditions (operator:value) of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_VERSION],
                'description' => 'REGEXP pattern version to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_VERSION, 'v'],
                'description' => 'Comma separated list of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_VERSION],
                'description' => 'Comma separated list of sort conditions (operator:value) of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_ORDER],
                'description' => 'REGEXP pattern order to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_ORDER, 'o'],
                'description' => 'Comma separated list of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_ORDER],
                'description' => 'Comma separated list of sort conditions (operator:value) of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED, 'e'],
                'description' => 'Select migrations already executed if = true, not executed if = false (by default), the both if = null',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string',
                'enum_value' => ['true', '1', 'false', '0', 'null']
            ]
        ]
    ],
    'migration_rollback_migration' => [
        'source' => 'migrate:rollback:migration',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionRollbackMigration'
        ],
        'description' => 'Rollback specified migration',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstMigration::COMMAND_ARG_NAME_KEY,
                'description' => 'Migration key',
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_FORCE, 'f'],
                'description' => 'Force access in case migration not already executed',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ]
    ],
    'migration_rollback' => [
        'source' => 'migrate:rollback',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionRollback'
        ],
        'description' => 'Rollback migrations',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_KEY],
                'description' => 'REGEXP pattern key to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_KEY, 'k'],
                'description' => 'Comma separated list of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_KEY],
                'description' => 'Comma separated list of sort conditions (operator:value) of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_VERSION],
                'description' => 'REGEXP pattern version to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_VERSION, 'v'],
                'description' => 'Comma separated list of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_VERSION],
                'description' => 'Comma separated list of sort conditions (operator:value) of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_ORDER],
                'description' => 'REGEXP pattern order to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_ORDER, 'o'],
                'description' => 'Comma separated list of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_ORDER],
                'description' => 'Comma separated list of sort conditions (operator:value) of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED, 'e'],
                'description' => 'Select migrations already executed if = true (by default), not executed if = false, the both if = null',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string',
                'enum_value' => ['true', '1', 'false', '0', 'null']
            ]
        ]
    ],
    'migration_refresh_migration' => [
        'source' => 'migrate:refresh:migration',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionRefreshMigration'
        ],
        'description' => 'Refresh specified migration (rollback and execute migration)',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstMigration::COMMAND_ARG_NAME_KEY,
                'description' => 'Migration key',
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_FORCE, 'f'],
                'description' => 'Force access in case migration not already executed',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ]
    ],
    'migration_refresh' => [
        'source' => 'migrate:refresh',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionRefresh'
        ],
        'description' => 'Refresh migrations (rollback and execute migrations)',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_KEY],
                'description' => 'REGEXP pattern key to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_KEY, 'k'],
                'description' => 'Comma separated list of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_KEY],
                'description' => 'Comma separated list of sort conditions (operator:value) of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_VERSION],
                'description' => 'REGEXP pattern version to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_VERSION, 'v'],
                'description' => 'Comma separated list of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_VERSION],
                'description' => 'Comma separated list of sort conditions (operator:value) of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_ORDER],
                'description' => 'REGEXP pattern order to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_ORDER, 'o'],
                'description' => 'Comma separated list of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_ORDER],
                'description' => 'Comma separated list of sort conditions (operator:value) of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_ROLLBACK_IS_EXECUTED, 'r'],
                'description' => 'Select migrations for rollback already executed if = true (by default), not executed if = false, the both if = null',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string',
                'enum_value' => ['true', '1', 'false', '0', 'null']
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_EXECUTE_IS_EXECUTED, 'e'],
                'description' => 'Select migrations for execute already executed if = true, not executed if = false (by default), the both if = null',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string',
                'enum_value' => ['true', '1', 'false', '0', 'null']
            ]
        ]
    ],
    'migration_show_migration' => [
        'source' => 'migrate:show:migration',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionShowMigration'
        ],
        'description' => 'Show specified migration',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstMigration::COMMAND_ARG_NAME_KEY,
                'description' => 'Migration key',
                'type_value' => 'string'
            ]
        ]
    ],
    'migration_show' => [
        'source' => 'migrate:show',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionShow'
        ],
        'description' => 'Show list of migrations',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_KEY],
                'description' => 'REGEXP pattern key to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_KEY, 'k'],
                'description' => 'Comma separated list of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_KEY],
                'description' => 'Comma separated list of sort conditions (operator:value) of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_VERSION],
                'description' => 'REGEXP pattern version to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_VERSION, 'v'],
                'description' => 'Comma separated list of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_VERSION],
                'description' => 'Comma separated list of sort conditions (operator:value) of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_ORDER],
                'description' => 'REGEXP pattern order to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_ORDER, 'o'],
                'description' => 'Comma separated list of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_ORDER],
                'description' => 'Comma separated list of sort conditions (operator:value) of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED, 'e'],
                'description' => 'Select migrations already executed if = true, not executed if = false, the both if = null (by default)',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string',
                'enum_value' => ['true', '1', 'false', '0', 'null']
            ]
        ]
    ],
    'migration_set_is_executed' => [
        'source' => 'migrate:set:is-executed',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionSetIsExecuted'
        ],
        'description' => '(Un)Mark migrations as is executed',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_KEY],
                'description' => 'REGEXP pattern key to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_KEY, 'k'],
                'description' => 'Comma separated list of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_KEY],
                'description' => 'Comma separated list of sort conditions (operator:value) of keys to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_VERSION],
                'description' => 'REGEXP pattern version to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_VERSION, 'v'],
                'description' => 'Comma separated list of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_VERSION],
                'description' => 'Comma separated list of sort conditions (operator:value) of versions to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_REGEXP_ORDER],
                'description' => 'REGEXP pattern order to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_LIST_ORDER, 'o'],
                'description' => 'Comma separated list of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SORT_COMPARE_ORDER],
                'description' => 'Comma separated list of sort conditions (operator:value) of orders to select',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_IS_EXECUTED, 'e'],
                'description' => 'Select migrations already executed if = true, not executed if = false (by default), the both if = null',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string',
                'enum_value' => ['true', '1', 'false', '0', 'null']
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_SET_IS_NOT_EXECUTED, 'n'],
                'description' => 'Unmark migrations as is executed',
                'required' => false,
                'required_value' => false,
                'type_value' => 'boolean'
            ]
        ]
    ],
    'migration_add' => [
        'source' => 'migrate:add',
        'call' => [
            'class_path_pattern' => MigrationController::class . ':actionAdd'
        ],
        'description' => 'Add specified new migration',
        'source_argument' => [
            [
                'type' => 'argument',
                'name' => ConstMigration::COMMAND_ARG_NAME_FILE_PATH,
                'description' => 'File full path, from application root directory.',
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_CLASS_PATH, 'c'],
                'description' => 'Class full path',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_PARENT_CLASS_PATH, 'p'],
                'description' => 'Parent class full path',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_KEY, 'k'],
                'description' => 'Migration key',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_VERSION, 'v'],
                'description' => 'Migration version',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ],
            [
                'type' => 'option',
                'name' => [ConstMigration::COMMAND_OPT_NAME_ORDER, 'o'],
                'description' => 'Migration order',
                'required' => false,
                'required_value' => true,
                'type_value' => 'integer'
            ]
        ]
    ]
);