LibertyCodeModule_Migration
===========================



Description
-----------

Library contains framework modules, 
to manage application migrations.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Framework module installation requirement
-----------------------------------------

1. Module repository: liberty_code_module/validation: version 1.0

    - Module validation (or equivalent).
    - Module rule (or equivalent).

2. Module repository: liberty_code_module/datetime: version 1.0

    - Module datetime (or equivalent).

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code_module/migration ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code_module/migration": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Application installation
------------------------

#### Configuration

1. Configuration: application module: "<project root path>/config/Module.<config_file_ext>"

    Add in list part, required modules:

    Example for YML configuration format, from composer installation:

    ```yml
    list: [
        {
            path: "/vendor/liberty_code_module/migration/src/migration",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        }
    ]
    ```

2. Configuration: DI: "<module root path>/config/Di.<config_file_ext>"

    - Minimum configuration required to get following class:
    
        - liberty_code\sql\persistence\table\model\TablePersistor

---



Configuration
-------------

#### Application parameters configuration

- Use following file on your modules to configure specific elements
    
    ```sh
    <module_root_path>/config/ParamApp.php
    ```

- Elements configurables

    - Configuration to populate migration collection

---



Usage
-----

Following examples consider command line application relative path as 
"bin/app".

#### Migration

Features allow to manage application migrations.

_Example_

```sh
cd "<project_root_path>"
...
// Show list of migrations
php bin/app migrate:show
...
// Execute migrations
migrate:execute
...
// Rollback migrations
migrate:rollback
```

---


