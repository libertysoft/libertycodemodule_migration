<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/migration/library/ConstMigration.php');
include($strRootPath . '/src/migration/library/ToolBoxMigration.php');
include($strRootPath . '/src/migration/library/ToolBoxRender.php');

include($strRootPath . '/src/migration/exception/FilePathAddInvalidFormatException.php');

include($strRootPath . '/src/migration/model/repository/MigrationCollectionRepository.php');

include($strRootPath . '/src/migration/command/model/CommandMigration.php');

include($strRootPath . '/src/migration/command/fix/model/FixCommandMigration.php');

include($strRootPath . '/src/migration/controller/command/MigrationController.php');

include($strRootPath . '/src/migration/boot/ToolBoxMigrationBootstrap.php');
include($strRootPath . '/src/migration/boot/MigrationBootstrap.php');